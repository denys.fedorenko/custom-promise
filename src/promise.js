const OwnPromise = (function() {
  const status = Symbol('Status');
  const result = Symbol('Result');
  const onFulfilledCallbacks = Symbol('ThenCallback');
  const onRejectCallbacks = Symbol('CatchCallback');
  const changePromiseStatus = Symbol('ChangePromiseStatus');
  const PENDING = 'pending';
  const REJECTED = 'rejected';
  const FULFILLED = 'fulfilled';

  class OwnPromise {
    [status] = PENDING;
    [result] = undefined;
    [onFulfilledCallbacks] = [];
    [onRejectCallbacks] = [];
    constructor(executor) {
      if (typeof this !== 'object' || typeof executor !== 'function' || this[status] !== PENDING) {
        throw new TypeError();
      }

      function rejectFunction(val) {
        if (this[status] !== PENDING) {
          throw new TypeError();
        }

        this[changePromiseStatus](REJECTED, val);
      }

      function resolveFunction(val) {
        if (this[status] !== PENDING) {
          throw new TypeError();
        }
        this[changePromiseStatus](FULFILLED, val);
      }

      try {
        executor(resolveFunction.bind(this), rejectFunction.bind(this));
      } catch (err) {
        rejectFunction.call(this, err);
      }
    }

    [changePromiseStatus](status, val) {
      setTimeout(() => {
        this[result] = val;
        this[status] = status;
        this[status === FULFILLED ? onFulfilledCallbacks : onRejectCallbacks].forEach(el => el(val));
        this[onFulfilledCallbacks] = [];
        this[onRejectCallbacks] = [];
      });
    }


    static resolve(val) {
      if (typeof this !== 'function' || this !== OwnPromise) {
        throw new TypeError();
      }

      if (val && val instanceof OwnPromise) {
        return val;
      }

      return new OwnPromise(res => {
        res(val);
      });
    }

    static reject(err) {
      if (typeof this !== 'function' || this !== OwnPromise) {
        throw new TypeError();
      }

      return new OwnPromise((res, rej) => {
        rej(err);
      });
    }

    then(onResolve, onReject) {
      if (this.constructor !== OwnPromise) {
        throw new TypeError();
      }

      if (this[status] === REJECTED && !onReject) {
        return this;
      }

      return new OwnPromise((res, rej) => {
        if (this[status] === FULFILLED) {
          res(onResolve(this[result]));
        } else if (this[status] === PENDING) {
          this[onFulfilledCallbacks].push(result => res(onResolve(result)));

          if (onReject) {
            this[onRejectCallbacks].push(err => res(onReject(err)));
          } else {
            this[onRejectCallbacks].push(err => rej(err));
          }
        } else {
          rej(onReject(this[result]));
        }
      });
    }

    catch(onReject) {
      return new OwnPromise((res, rej) => {
        if (this[status] === REJECTED) {
          res(onReject(this[result]));
        } else {
          this[onRejectCallbacks].push(result => rej(onReject(result)));
        }
      });
    }

    static all(iterable) {
      if (this !== OwnPromise) {
        throw new TypeError();
      }

      if (!(Symbol.iterator in Object(iterable))) {
        return OwnPromise.reject(new TypeError('non-iterable'));
      }

      if (iterable.length === 0) {
        return new OwnPromise(res => res([]));
      }

      return new OwnPromise((res, rej) => {
        const results = [];
        let completeTask = 0;

        iterable.forEach(value => {
          OwnPromise.resolve(value)
            .then(result => {
              results.push(result);
              completeTask += 1;

              if (completeTask === iterable.length) {
                setTimeout(() => {
                  res(results);
                }, 0);
              }
            })
            .catch(rej);
        });
      });
    }

    static race(iterable) {
      if (this !== OwnPromise) {
        throw new TypeError();
      }
      return new OwnPromise((res, rej) => {
        iterable.forEach(el => {
          setTimeout(() => {
            el.then(res, rej);
          });
        });
      });
    }
  }

  return OwnPromise;
}());

module.exports = OwnPromise;
