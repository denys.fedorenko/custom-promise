const { assert, expect } = require('chai');
const OwnPromise = require('../src/promise');

describe('1. The Promise Constructor', function() {
  it('1.1. is a function', function() {
    assert.isFunction(OwnPromise.prototype.constructor);
  });

  it('1.2. can be called as a function', function() {
    assert.isFunction(OwnPromise.prototype.constructor);
  });
});
