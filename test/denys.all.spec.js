const { assert, expect } = require('chai');
const OwnPromise = require('../src/promise');

describe('1. Promise.all', function() {
  it('1.1. returned array with undefined', function(done) {
    const iterable = [undefined, undefined, undefined, undefined];
    OwnPromise.all(iterable).then(function(res) {
      expect(res[0]).to.be.undefined;
      expect(res[1]).to.be.undefined;
      expect(res[3]).to.be.undefined;
    })
      .then(done)
      .catch(done);
  });

  it('1.2. returned array with null', function(done) {
    const iterable = [null, null, null, null];
    OwnPromise.all(iterable).then(function(res) {
      expect(res[0]).to.be.null;
      expect(res[1]).to.be.null;
      expect(res[3]).to.be.null;
    })
      .then(done)
      .catch(done);
  });
});
