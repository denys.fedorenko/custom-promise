const { assert, expect } = require('chai');
const OwnPromise = require('../src/promise');

describe('25.4.5.1 Promise.prototype.catch( onRejected )', function() {
  it('is a function', function() {
    assert.isFunction(OwnPromise.prototype.catch);
  });
  it('is a function', function() {
    const promise = new OwnPromise(() => null);
    expect(promise).to.not.have.ownProperty('catch');
  });
  it('is a function', function() {
    assert.lengthOf(OwnPromise.prototype.catch, 1);
  });

  describe('Own properties', function() {
    it('Instance of Promise doesnt have any Own properties', function() {
      const promise = new OwnPromise(() => null);
      assert.lengthOf(Object.getOwnPropertyNames(promise), 0);
    });
    it('Promises prototype has only declarated own properties', function() {
      expect(Object.getOwnPropertyNames(OwnPromise.prototype), ['constructor', 'then', 'catch', 'finally']);
    });
    it('Promises has static properties', function() {
      expect(OwnPromise).to.haveOwnProperty('all');
      expect(OwnPromise).to.haveOwnProperty('race');
      expect(OwnPromise).to.haveOwnProperty('resolve');
      expect(OwnPromise).to.haveOwnProperty('reject');
    });
  });
});