const { assert, expect } = require('chai');
const OwnPromise = require('../src/promise');

describe('1. Promise.prototype', function() {
  it('1.1. has attribute [[Writable]]: false', function() {
    expect(OwnPromise).to.have.ownPropertyDescriptor('prototype').that.has.property('writable', false);
  });

  it('1.2. has attribute [[Enumerable]]: false', function() {
    expect(OwnPromise).to.have.ownPropertyDescriptor('prototype').that.has.property('enumerable', false);
  });

  it('1.3. has attribute [[Configurable]]: false', function() {
    expect(OwnPromise).to.have.ownPropertyDescriptor('prototype').that.has.property('configurable', false);
  });
});

describe('2. Promise.prototype.constructor', function() {
  it('2.1. is an object', function() {
    assert.ok(OwnPromise.prototype.constructor instanceof Object);
  });

  it('2.2. is a function', function() {
    assert.isFunction(OwnPromise.prototype.constructor);
  });
});

describe('3. Promise.prototype.then', function() {
  it('3.1. is a function', function() {
    assert.isFunction(OwnPromise.prototype.then);
  });

  it('3.2. expects "this" to be a Promise', function() {
    const promise = new OwnPromise(resolve => resolve('promise'));

    promise.then(function() {
      assert.equal(this, OwnPromise);
    });
  });

  it('3.3. throws TypeError if "this" is not a Promise', function() {
    const notAConstructor = [];
    const promise = new OwnPromise(() => null);

    assert.throws(function() {
      promise.then.call(notAConstructor, 3);
    }, TypeError);
  });
});
