const { assert, expect, done } = require('chai');
const OwnPromise = require('../src/promise');

describe('1. Promise.race', function () {
  it('1.1. array of promises should return result promises', function (done) {
    const iterable = ['some string', 100, undefined, 0];
    OwnPromise.race(iterable).then(function (res) {
      expect(res).to.equal('some string');
    })
      .then(done)
      .catch(done);
  });
});
